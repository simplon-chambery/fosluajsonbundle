<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Processor;

use FOS\Bundle\LuaJsonBundle\Factory\JsonFactoryInterface;
use FOS\Bundle\LuaJsonBundle\Factory\LuaSandboxFunctionFactoryInterface;
use FOS\Bundle\LuaJsonBundle\Model\Result;
use FOS\Bundle\LuaJsonBundle\Model\ResultInterface;
use FOS\Bundle\LuaJsonBundle\Validator\JsonValidatorInterface;

final class LuaJsonProcessor implements LuaJsonProcessorInterface
{
    /**
     * @var JsonValidatorInterface
     */
    private $jsonValidator;

    /**
     * @return JsonValidatorInterface
     */
    public function getJsonValidator(): JsonValidatorInterface
    {
        return $this->jsonValidator;
    }

    /**
     * @var JsonFactoryInterface
     */
    private $jsonFactory;

    /**
     * @var LuaSandboxFunctionFactoryInterface
     */
    private $luaSandboxFunctionFactory;

    /**
     * LuaJsonProcessor constructor.
     * @param JsonValidatorInterface $jsonValidator
     * @param JsonFactoryInterface $jsonFactory
     * @param LuaSandboxFunctionFactoryInterface $luaSandboxFunctionFactory
     */
    public function __construct(JsonValidatorInterface $jsonValidator, JsonFactoryInterface $jsonFactory, LuaSandboxFunctionFactoryInterface $luaSandboxFunctionFactory)
    {
        $this->jsonValidator = $jsonValidator;
        $this->jsonFactory = $jsonFactory;
        $this->luaSandboxFunctionFactory = $luaSandboxFunctionFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute(string $json, string $schema, string $code): ResultInterface
    {
        $result = new Result();

        $json = $this->jsonFactory->createJson($json, $schema);
        if(!$this->jsonValidator->isValid($json)) {
            $result->setErrors(
                $this->jsonValidator->getErrors()
            );
            return $result;
        }


        $luaSandboxFunction = $this->luaSandboxFunctionFactory->createFromString($code);
        $data = $luaSandboxFunction->call(
            json_decode($json->getData(), true)
        );

        return $result->setData($data[0]);
    }
}