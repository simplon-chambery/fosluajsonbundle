<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Processor;

use FOS\Bundle\LuaJsonBundle\Model\ResultInterface;

/**
 * Interface LuaJsonProcessorInterface
 * @package FOS\Bundle\LuaJsonBundle\Processor
 */
interface LuaJsonProcessorInterface
{
    /**
     * @param string $json
     * @param string $schema
     * @param string $code
     * @return mixed
     */
    public function execute(string $json, string $schema, string $code): ResultInterface;
}