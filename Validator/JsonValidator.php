<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Validator;

use FOS\Bundle\LuaJsonBundle\Model\JsonInterface;
use Opis\JsonSchema\IValidator;
use Opis\JsonSchema\ValidationResult;

/**
 * Class JsonValidator
 * @package FOS\Bundle\LuaJsonBundle\Validator
 */
final class JsonValidator implements JsonValidatorInterface
{
    /**
     * @var IValidator
     */
    private $validator;

    /**
     * @var ValidationResult
     */
    private $result;

    /**
     * JsonValidator constructor.
     * @param IValidator $validator
     */
    public function __construct(IValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @inheritDoc
     */
    public function isValid(JsonInterface $json): bool
    {
       $this->result = $this->validator->schemaValidation(
           json_decode($json->getData()), $json->getSchema()
       );

       return $this->result->isValid();
    }

    /**
     * @inheritDoc
     */
    public function getResult(): ?ValidationResult
    {
        return $this->result;
    }

    /**
     * @inheritDoc
     */
    public function getErrors(): array
    {
        return $this->result ? $this->result->getErrors() : [];
    }
}