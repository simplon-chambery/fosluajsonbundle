<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Validator;

use FOS\Bundle\LuaJsonBundle\Model\JsonInterface;
use Opis\JsonSchema\ValidationResult;

/**
 * Interface JsonValidatorInterface
 * @package FOS\Bundle\LuaJsonBundle\Validator
 */
interface JsonValidatorInterface
{
    /**
     * @param JsonInterface $json
     * @return bool
     */
    public function isValid(JsonInterface $json): bool;

    /**
     * @return ValidationResult|null
     */
    public function getResult(): ?ValidationResult;

    /**
     * @return array
     */
    public function getErrors(): array;
}