<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class FOSLuaJsonBundle
 */
final class FOSLuaJsonBundle extends Bundle
{

}