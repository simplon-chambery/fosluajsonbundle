<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Model;

use Opis\JsonSchema\ISchema;

/**
 * Interface JsonInterface
 * @package FOS\Bundle\LuaJsonBundle\Model
 */
interface JsonInterface
{
    /**
     * @return string
     */
    public function getData(): string;

    /**
     * @param string $data
     * @return JsonInterface
     */
    public function setData(string $data): self;

    /**
     * @return ISchema
     */
    public function getSchema(): ISchema;

    /**
     * @param ISchema $schema
     * @return JsonInterface
     */
    public function setSchema(ISchema $schema): self;
}