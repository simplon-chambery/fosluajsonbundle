<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Model;

/**
 * Class Result
 * @package FOS\Bundle\LuaJsonBundle\Model
 */
class Result implements ResultInterface
{
    /**
     * @var string
     */
    private $data;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @inheritDoc
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @inheritDoc
     */
    public function setData($data): ResultInterface
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @inheritDoc
     */
    public function setErrors(array $errors): ResultInterface
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return sizeof($this->errors) > 0;
    }
}