<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Model;

/**
 * Interface ResultInterface
 * @package FOS\Bundle\LuaJsonBundle\Model
 */
interface ResultInterface
{
    /**
     * @return mixed
     */
    public function getData();

    /**
     * @param $data
     * @return mixed
     */
    public function setData($data): self;

    /**
     * @return array
     */
    public function getErrors(): array;

    /**
     * @param array $errors
     * @return ResultInterface
     */
    public function setErrors(array $errors): self;

    /**
     * @return bool
     */
    public function isValid(): bool;
}