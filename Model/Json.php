<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Model;

use Opis\JsonSchema\ISchema;

/**
 * Class Json
 * @package FOS\Bundle\LuaJsonBundle\Model
 */
class Json implements JsonInterface
{
    /**
     * @var string
     */
    private $data;

    /**
     * @var ISchema
     */
    private $schema;

    /**
     * @inheritDoc
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @inheritDoc
     */
    public function setData(string $data): JsonInterface
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSchema(): ISchema
    {
        return $this->schema;
    }

    /**
     * @inheritDoc
     */
    public function setSchema(ISchema $schema): JsonInterface
    {
        $this->schema = $schema;

        return $this;
    }
}