<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Factory;

use LuaSandbox;
use LuaSandboxFunction;

/**
 * Class LuaSandboxFunctionFactory
 * @package FOS\Bundle\LuaJsonBundle\Factory
 */
final class LuaSandboxFunctionFactory implements LuaSandboxFunctionFactoryInterface
{
    /**
     * @var LuaSandbox
     */
    private $luaSandbox;

    /**
     * LuaSandboxFunctionFactory constructor.
     * @param LuaSandbox $luaSandbox
     */
    public function __construct(LuaSandbox $luaSandbox)
    {
        $this->luaSandbox = $luaSandbox;
    }

    /**
     * @inheritDoc
     */
    public function createFromString(string $code): LuaSandboxFunction
    {
        list( $luaFunction ) = $this->luaSandbox->loadString(
            sprintf('return %s', $code)
        )->call();

        return $luaFunction;
    }
}