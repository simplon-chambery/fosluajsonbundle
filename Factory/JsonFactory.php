<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Factory;

use FOS\Bundle\LuaJsonBundle\Model\Json;
use FOS\Bundle\LuaJsonBundle\Model\JsonInterface;
use Opis\JsonSchema\Schema;

/**
 * Class JsonFactory
 * @package FOS\Bundle\LuaJsonBundle\Factory
 */
final class JsonFactory implements JsonFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createJson(string $data, string $schema): JsonInterface
    {
        if(file_exists($data)) {
            $data = file_get_contents($data);
        }

        if(file_exists($schema)) {
            $schema = file_get_contents($schema);
        }

        $json = new Json();

        $json
            ->setData($data)
            ->setSchema(Schema::fromJsonString($schema))
        ;

        return $json;
    }
}