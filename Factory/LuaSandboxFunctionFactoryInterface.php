<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Factory;

use LuaSandboxFunction;

/**
 * Interface LuaSandboxFunctionFactoryInterface
 * @package FOS\Bundle\LuaJsonBundle\Factory
 */
interface LuaSandboxFunctionFactoryInterface
{
    public function createFromString(string $code): LuaSandboxFunction;
}