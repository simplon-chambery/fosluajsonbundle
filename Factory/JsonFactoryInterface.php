<?php

/*
 * This file is part of the FOS package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace FOS\Bundle\LuaJsonBundle\Factory;

use FOS\Bundle\LuaJsonBundle\Model\JsonInterface;

/**
 * Interface JsonFactoryInterface
 * @package FOS\Bundle\LuaJsonBundle\Factory
 */
interface JsonFactoryInterface
{
    /**
     * @param string $data
     * @param string $schema
     * @return JsonInterface
     */
    public function createJson(string $data, string $schema): JsonInterface;
}