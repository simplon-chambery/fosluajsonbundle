# Installation

You could directly reference it into your composer.json file as a dependency
```json
{
    "require": {
        "fos/lua-json-bundle": "0.*"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/simplon-chambery/fosluajsonbundle.git"
        }
    ]
}
```

## Example

```php
$schema = '{ 
    "type": "object" 
}';

$json = '{
   "key" : 10,
   "another_key" : "another_value"
}';

$code = '
    function ( json )
       return json["key"] + 10;
    end
';

/** @var Symfony\Component\DependencyInjection\ContainerInterface $container */
$result = $container->get(LuaJsonProcessor::class)->execute(
    $json, $schema, $code
);

/** display "20" */
echo $result->getData();

```